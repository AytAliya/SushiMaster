//
//  Sushi.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import Foundation

class Sushi{
    
    var mainImgName: String
    var sushiName: String
    var ingredients: String
    var info: String
    var fact: String
    var additional: String
    var order: Int
    var videoType: String
    var video: Video
    
    init(mainImgName: String, sushiName: String, ingredients: String, info: String, fact: String, additional: String, order: Int, videoType: String, video: Video) {
        self.mainImgName = mainImgName
        self.sushiName = sushiName
        self.ingredients = ingredients
        self.info = info
        self.fact = fact
        self.additional = additional
        self.order = order
        self.videoType = videoType
        self.video = video
    }
    
    static func fetchSushi() -> [Sushi] {
        return [
            Sushi(mainImgName: "s1",
                  sushiName: "Philadelphia roll",
                  ingredients: "Main Ingredients: Salmon, Cucumber, Rice, Nori, Cream cheese", info: "One of the well-known sushi rolls that consists of raw salmon and cream cheese.",
                  fact: "The name came from the name of the cream cheese Philadelphia.",
                  additional: "Other additional ingredients: avocado, asparagus", order: 1,
                  videoType: "Refers to a type of Uramaki",
                  video: Video(typeName: "Uramaki", imgName: "s1", video: "fila", info: "Uramaki are inside out rolls. They are made in such a way that rice is outside and the filling is in the center, wrapped  in the nori. There are some vegetables or fish at the top of the roll or envelop roll  in sesame, caviar.",thisType: "In this video as an example for Uramaki was taken roll Philadelphia ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Salmon at the top and Philadelphia cheese as a filling.", additional: "Other famous types of Uramaki: California, Dragon maki and etc.")),
            Sushi(mainImgName: "s2",
                  sushiName: "California roll",
                  ingredients: "Main Ingredients: Crab, Cucumber, Nori, Sesame seeds, Rice",
                  info: "California roll was originally created in the U.S, however now it has its own fan base in Japan and other parts of the world.",
                  fact: "It got the name \"California roll\" because it was very popular in Los Angeles.",
                  additional: "Other additional ingredients: Avocado, Tobico caviar", order: 2,
                  videoType: "Refers to a type of Uramaki",
                  video: Video(typeName: "Uramaki", imgName: "s1", video: "fila", info: "Uramaki are inside out rolls. They are made in such a way that rice is outside and the filling is in the center, wrapped  in the nori. There are some vegetables or fish at the top of the roll or envelop roll  in sesame, caviar.",thisType: "In this video as an example for Uramaki was taken roll Philadelphia ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Salmon at the top and Philadelphia cheese as a filling.", additional: "Other famous types of Uramaki: California, Dragon maki and etc.")),
            Sushi(mainImgName: "s3",
                  sushiName: "Dragon roll",
                  ingredients: "Main Ingredients: Eel, Crab, Rice, Cucumber and Avocado outside",
                  info: "Dragon roll is number one pick for an eel lovers.",
                  fact: "Eels is a sea snake, which resembles dragons. So that’s how Dragon roll got it’s name.",
                  additional: "",
                  order: 3,
                  videoType: "Refers to a type of Uramaki",
                  video: Video(typeName: "Uramaki", imgName: "s1", video: "fila", info: "Uramaki are inside out rolls. They are made in such a way that rice is outside and the filling is in the center, wrapped  in the nori. There are some vegetables or fish at the top of the roll or envelop roll  in sesame, caviar.",thisType: "In this video as an example for Uramaki was taken roll Philadelphia ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Salmon at the top and Philadelphia cheese as a filling.", additional: "Other famous types of Uramaki: California, Dragon maki and etc.")),
            Sushi(mainImgName: "s4",
                  sushiName: "Spicy tuna",
                  ingredients: "Main Ingredients: Tuna, Spicy sauce, Nori, Rice",
                  info: "The best choice for people who fancy a bit of spies in their life.",
                  fact: "Got it’s name from the main ingredients consisting only tuna and spicy sauce.",
                  additional: "Other additional ingredients: Mayo, Cucumber",
                  order: 4,
                  videoType: "Refers to a type of Hosomaki",
                  video: Video(typeName: "Hosomaki", imgName: "s4", video: "salmon", info: "Hosomaki is a type of sushi which consists of only three ingredients: rice, nori and filling. Filling should be simple and consist of only one type of fish or cream cheese or vegetable etc.", thisType: "In this video as an example for Hosomaki was taken roll Salmon maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and Salmon as a filling", additional: "Other famous types of Hosomaki: Kappa maki, spicy tuna and etc.")),
            Sushi(mainImgName: "s5",
                  sushiName: "Kappa Maki",
                  ingredients: "Main Ingredients: Cucumber, Nori, Japanese Rice",
                  info: "Cucumber rolls called Kappa maki in Japan, perfect for vegetarians.",
                  fact: "The name for this roll came from Japan, from the word kappa(imp figure from Japanese traditional folklore) associated with cucumber, but not meaning.(Cucumber in japanese is kyuuri)",
                  additional: "",
                  order: 5,
                  videoType: "Refers to a type of Hosomaki",
                  video: Video(typeName: "Hosomaki", imgName: "s4", video: "salmon", info: "Hosomaki is a type of sushi which consists of only three ingredients: rice, nori and filling. Filling should be simple and consist of only one type of fish or cream cheese or vegetable etc.", thisType: "In this video as an example for Hosomaki was taken roll Salmon maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and Salmon as a filling", additional: "Other famous types of Hosomaki: Kappa maki, spicy tuna and etc.")),
            Sushi(mainImgName: "s6",
                  sushiName: "Unagi maki",
                  ingredients: "Main Ingredients: Cucumber, Nori, Rice, Freshwater eel",
                  info: "Unagi maki is a good dish for non-fish eaters to try sushi, as it’s prepared with fresh water eel.",
                  fact: "The name for this roll is came from the japanese word Unagi meaning freshwater eel(especially Japanese)",
                  additional: "",
                  order: 6,
                  videoType: "Refers to a type of Futomaki",
                  video: Video(typeName: "Futomaki", imgName: "s9", video: "planeta", info: "Futomaki are big-size rolls with several fillings in different combinations. For instance: salmon with avocado and cream cheese or tuna with cucmber.", thisType: "In this video as an example for Futomaki was taken roll Salmon Takuan Maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and as a filling were used: lettuce, takuan, smoked eel and Philadelphia cheese.", additional: "Other famous types of Futomaki: Unagi maki, Yokohama and etc.")),
            Sushi(mainImgName: "s7",
                  sushiName: "Spider roll",
                  ingredients: "Main Ingredients: Soft-shell crab, Cucumber, Nori, Rice, Fresh lettuce, Mayo, Avocado",
                  info: "Spider roll is one of the unusual type of sushi, having the mix of vegetables with Soft-shell crab.",
                  fact: "It’s name was inspired from it’s arachnid appearance.",
                  additional: "",
                  order: 7,
                  videoType: "Refers to a type of Futomaki",
                  video: Video(typeName: "Futomaki", imgName: "s9", video: "planeta", info: "Futomaki are big-size rolls with several fillings in different combinations. For instance: salmon with avocado and cream cheese or tuna with cucmber.", thisType: "In this video as an example for Futomaki was taken roll Salmon Takuan Maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and as a filling were used: lettuce, takuan, smoked eel and Philadelphia cheese.", additional: "Other famous types of Futomaki: Unagi maki, Yokohama and etc.")),
            Sushi(mainImgName: "s8",
                  sushiName: "Caterpillar roll",
                  ingredients: "Main Ingredients: Crab, Cucumber, Nori, Rice, Avocado Outside, Salad",
                  info: "The type of sushi that is simple to make, beautiful to look at and delicious to eat.",
                  fact: "This roll gets its name from the avocado shell that looks like a caterpillar",
                  additional: "Other additional ingredients: smoked eel, boiled egg",
                  order: 8,
                  videoType: "Refers to a type of Uramaki",
                  video: Video(typeName: "Uramaki", imgName: "s1", video: "fila", info: "Uramaki are inside out rolls. They are made in such a way that rice is outside and the filling is in the center, wrapped  in the nori. There are some vegetables or fish at the top of the roll or envelop roll  in sesame, caviar.",thisType: "In this video as an example for Uramaki was taken roll Philadelphia ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Salmon at the top and Philadelphia cheese as a filling.", additional: "Other famous types of Uramaki: California, Dragon maki and etc.")),
            Sushi(mainImgName: "s9",
                  sushiName: "Salmon Takuan Maki",
                  ingredients: "Main Ingredients: Takuan, Salmon, Nori, Rice, Salad",
                  info: "One of the unusual type of sushi to taste because of takuan, which is pickled daikon radish",
                  fact: "Got it’s name from having takuan and salmon as it’s main ingredients.",
                  additional: "Other additional ingredients: Philadelphia cream cheese",
                  order: 9,
                  videoType: "Refers to a type of Futomaki",
                  video: Video(typeName: "Futomaki", imgName: "s9", video: "planeta", info: "Futomaki are big-size rolls with several fillings in different combinations. For instance: salmon with avocado and cream cheese or tuna with cucmber.", thisType: "In this video as an example for Futomaki was taken roll Salmon Takuan Maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and as a filling were used: lettuce, takuan, smoked eel and Philadelphia cheese.", additional: "Other famous types of Futomaki: Unagi maki, Yokohama and etc.")),
            Sushi(mainImgName: "s10",
                  sushiName: "Crunch Sushi Roll",
                  ingredients: "Main Ingredients: Spicy tuna, Crispy seaweed, Tempura, Rice",
                  info: "The type of sushi that is served being hot and crunch because of crispy tempura that surrounds the roll",
                  fact: "It’s name is inspired by the crispy tempura outside the sushi",
                  additional: "",
                  order: 10,
                  videoType: "Refers to a type of Futomaki",
                  video: Video(typeName: "Futomaki", imgName: "s9", video: "planeta", info: "Futomaki are big-size rolls with several fillings in different combinations. For instance: salmon with avocado and cream cheese or tuna with cucmber.", thisType: "In this video as an example for Futomaki was taken roll Salmon Takuan Maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and as a filling were used: lettuce, takuan, smoked eel and Philadelphia cheese.", additional: "Other famous types of Futomaki: Unagi maki, Yokohama and etc.")),
            Sushi(mainImgName: "s11",
                  sushiName: "Nigiri",
                  ingredients: "Main Ingredients: Rice and Salmon/Eel/Tuna/Shrimp above the rice",
                  info: "Is different type of sushi, which vary most obviously by shape and construction.The rise in this type is pressed between the palms of the hands to form an oval-shaped ball.",
                  fact: "Got it’s name from the Japanese word Nigiri meaning hand-pressed.",
                  additional: "",
                  order: 11,
                  videoType: "Refers to a type of Nigiri",
                  video: Video(typeName: "Nigiri", imgName: "s11", video: "nigiri", info: "Nigiri - the simplest and the easiest type of sushi that can be cooked. It represents a small lump of rice with a piece of fish on top. Sometimes in order to avoid the falling of filling chefs fix the roll with a strip of nori.",thisType: "In this video as examples for Nigiri were taken Nigiri with eel and Nigiri with prawn. ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Glass of water and for the filling: prawn or smoked eel.", additional: "Other famous types of Nigiri: Nigiri with salmon and Nigiri with tuna, etc.")),
            Sushi(mainImgName: "s12",
                  sushiName: "Gunkan maki",
                  ingredients: "Main Ingredients:  Rice, Nori, Fillings above as Red Caviar, Tobiko Caviar, Chuka Seaweed and etc.",
                  info: "The type of sushi that is oval shaped, having nori wrapped around it’s perimeter.",
                  fact: "The name came from Japanese word Gunkan meaning boat, as the sushi looks like.",
                  additional: "",
                  order: 12,
                  videoType: "Refers to a type of Gunkan",
                  video: Video(typeName: "Gunkan", imgName: "s12", video: "gunkan", info: "Gunkan are the owl-shaped sushi, where rice is wrapped with a wide strip of nori, the filling is laid out along the perimeter and the top.", thisType: "In this video as an example for Gunkan was taken Gunkan Chuka.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and for the filling: Chuka and sesame seeds.", additional: "Other famous types of Gunkan: Gunkan with red caviar, Gunkan with salmon, etc."))
        ]
    }
}


