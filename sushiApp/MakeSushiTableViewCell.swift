//
//  MakeSushiTableViewCell.swift
//  sushiApp
//
//  Created by Aliya on 23.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit

class MakeSushiTableViewCell: UITableViewCell {
    
    var typeNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(name: "Savoye LET", size: 40)
        return label
    }()
    

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews(){
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.addSubview( typeNameLabel)
    }


}
