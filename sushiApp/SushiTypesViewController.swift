//
//  SushiTypesViewController.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class SushiTypesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView = UITableView()
    
    var items = Video.fetchVideo()
    
    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "sushiBg")
        backImage.clipsToBounds = true
        backImage.alpha = 0.5
        return backImage
    }()

    let screen = UIScreen.main.bounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubviews(backImage)
        setupConstraints()
        tableSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow
        {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableSettings(){
        
        let navBar = self.navigationController?.navigationBar.frame.size.height
        tableView = UITableView(frame: CGRect(x: 10, y: navBar!+8, width: screen.width-20, height: screen.height - 60), style: UITableViewStyle.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(VideoTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.separatorColor = UIColor.clear.withAlphaComponent(0)
        //tableView.allowsSelection = false
        self.view.addSubview(tableView)
    }
    
    func setupConstraints(){
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! VideoTableViewCell
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = #colorLiteral(red: 0.4159462246, green: 0.4159462246, blue: 0.4159462246, alpha: 0.6034621147)
        cell.selectedBackgroundView = bgColorView
        
        cell.sushiImg.image = UIImage(named: self.items[indexPath.row].imgName)
        cell.sushiImg <- [
            Left(screen.width*20/414).to(cell, .left),
            CenterY().to(cell, .centerY),
            Width(screen.width/4-20),
            Height(screen.width/4-20)
        ]
        cell.typeNameLabel.text = self.items[indexPath.row].typeName.localized()
        cell.typeNameLabel.font = UIFont(name: "Savoye LET", size: 40)
        
        cell.typeNameLabel <- [
            CenterX(20).to(cell, .centerX),
            CenterY(10).to(cell, .centerY)
        ]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (screen.height - 60)/5
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoView = SushiVideoTutorialsViewController()
        videoView.sushiVideo = items[indexPath.row]
        self.navigationController?.pushViewController(videoView, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
