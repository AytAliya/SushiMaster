//
//  AllSushiPageViewController.swift
//  sushiApp
//
//  Created by Aliya on 14.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class AllSushiPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    var pages = [UIViewController]()
    let pageControl = MyPageControl()
    var sushiArray = Sushi.fetchSushi()
    let page1 = SushiInfoViewController()
    let page2 = SushiInfoViewController()
    let page3 = SushiInfoViewController()
    let page4 = SushiInfoViewController()
    let page5 = SushiInfoViewController()
    let page6 = SushiInfoViewController()
    let page7 = SushiInfoViewController()
    let page8 = SushiInfoViewController()
    let page9 = SushiInfoViewController()
    let page10 = SushiInfoViewController()
    let page11 = SushiInfoViewController()
    let page12 = SushiInfoViewController()
    
    var al:UIAlertController = {
        let alert = UIAlertController()
        alert.message = "Tap the elements on the screen to see the information"
        alert.view.tintColor = .red
        alert.view.frame.origin = CGPoint(x: 0, y: 0)
        return alert
    }()
    
    var infoBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "infoIcon"), for: .normal)
        btn.addTarget(self, action: #selector(showAlert), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        addPages()
        view.addSubview(infoBtn)
        infoBtn <- [
            Height(view.bounds.width*20/375),
            Width(view.bounds.width*20/375),
            Top(50),
            Left(8)
        ]
        self.view.addSubview(self.pageControl)
        automaticallyAdjustsScrollViewInsets = false
        self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //showAlert()
    }
    
    func addPages(){
        page1.sushi = sushiArray[0]
        page2.sushi = sushiArray[1]
        page3.sushi = sushiArray[2]
        page4.sushi = sushiArray[3]
        page5.sushi = sushiArray[4]
        page6.sushi = sushiArray[5]
        page7.sushi = sushiArray[6]
        page8.sushi = sushiArray[7]
        page9.sushi = sushiArray[8]
        page10.sushi = sushiArray[9]
        page11.sushi = sushiArray[10]
        page12.sushi = sushiArray[11]
        [page1, page2, page3, page4, page5, page6, page7, page8, page9, page10, page11, page12].forEach{ page in
                self.pages.append(page)
            }
        setViewControllers([pages[0]], direction: .forward, animated: true, completion: nil)
        self.pageControl.numberOfPages = self.pages.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.pages.index(of: viewController) {
            if viewControllerIndex == 0 {
                return self.pages[11]
            } else {
                return self.pages[viewControllerIndex - 1]
            }
        }
        return nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let viewControllerIndex = self.pages.index(of: viewController) {
            if viewControllerIndex < self.pages.count - 1 {
                return self.pages[viewControllerIndex + 1]
            } else if viewControllerIndex == self.pages.count - 1{
                return self.pages[0]
            }
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if let viewControllers = pageViewController.viewControllers {
            if let viewControllerIndex = self.pages.index(of: viewControllers[0]) {
                self.pageControl.currentPage = viewControllerIndex
            } 
        }
    }
    
    func showAlert() {
        var messageFont = [NSFontAttributeName: UIFont(name: "AvenirNext-Bold", size: 15.0)!]
        if(UIScreen.main.bounds.height < 481){
            messageFont = [NSFontAttributeName: UIFont(name: "AvenirNext-Bold", size: 13.0)!]
        }
        let messageAttrString = NSMutableAttributedString(string: "<- Swipe the views ->".localized(), attributes: messageFont)
        al.setValue(messageAttrString, forKey: "attributedMessage")
        self.present(al, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { _ in self.al.dismiss(animated: true, completion: nil)} )
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}


class MyPageControl: UIPageControl{
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        isHidden = true
        currentPageIndicatorTintColor = #colorLiteral(red: 0.9035408696, green: 0.9035408696, blue: 0.9035408696, alpha: 0.8980896832)
        pageIndicatorTintColor = UIColor.black
        currentPage = 0
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

