//
//  Video.swift
//  sushiApp
//
//  Created by Aliya on 09.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import Foundation

class Video{
    
    var typeName: String
    var imgName: String
    var video: String
    var info: String
    var thisType: String
    var ingredients: String
    var additional: String
    
    init(typeName: String, imgName: String, video: String, info: String, thisType: String, ingredients: String, additional: String) {
        self.typeName = typeName
        self.imgName = imgName
        self.video = video
        self.info = info
        self.thisType = thisType
        self.ingredients = ingredients
        self.additional = additional
    }
    
    static func fetchVideo() -> [Video] {
    return [
        Video(typeName: "Hosomaki", imgName: "s4", video: "salmon", info: "Hosomaki is a type of sushi which consists of only three ingredients: rice, nori and filling. Filling should be simple and consist of only one type of fish or cream cheese or vegetable etc.", thisType: "In this video as an example for Hosomaki was taken roll Salmon maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and Salmon as a filling", additional: "Other famous types of Hosomaki: Kappa maki, spicy tuna and etc."),
        Video(typeName: "Futomaki", imgName: "s9", video: "planeta", info: "Futomaki are big-size rolls with several fillings in different combinations. For instance: salmon with avocado and cream cheese or tuna with cucmber.", thisType: "In this video as an example for Futomaki was taken roll Salmon Takuan Maki.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and as a filling were used: lettuce, takuan, smoked eel and Philadelphia cheese.", additional: "Other famous types of Futomaki: Unagi maki, Yokohama and etc."),
        Video(typeName: "Uramaki", imgName: "s1", video: "fila", info: "Uramaki are inside out rolls. They are made in such a way that rice is outside and the filling is in the center, wrapped  in the nori. There are some vegetables or fish at the top of the roll or envelop roll  in sesame, caviar.",thisType: "In this video as an example for Uramaki was taken roll Philadelphia ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Salmon at the top and Philadelphia cheese as a filling.", additional: "Other famous types of Uramaki: California, Dragon maki and etc."),
        Video(typeName: "Nigiri", imgName: "s11", video: "nigiri", info: "Nigiri - the simplest and the easiest type of sushi that can be cooked. It represents a small lump of rice with a piece of fish on top. Sometimes in order to avoid the falling of filling chefs fix the roll with a strip of nori.",thisType: "In this video as examples for Nigiri were taken Nigiri with eel and Nigiri with prawn. ", ingredients: "The ingredients that were used in the  video: Japanese rice, Nori, Glass of water and for the filling: prawn or smoked eel.", additional: "Other famous types of Nigiri: Nigiri with salmon and Nigiri with tuna, etc."),
        Video(typeName: "Gunkan", imgName: "s12", video: "gunkan", info: "Gunkan are the owl-shaped sushi, where rice is wrapped with a wide strip of nori, the filling is laid out along the perimeter and the top.", thisType: "In this video as an example for Gunkan was taken Gunkan Chuka.", ingredients: "The ingredients that were used in the video: Japanese rice, Nori, Glass of water and for the filling: Chuka and sesame seeds.", additional: "Other famous types of Gunkan: Gunkan with red caviar, Gunkan with salmon, etc.")
        ]
    }
}
