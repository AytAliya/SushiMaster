//
//  GameViewController.swift
//  sushiApp
//
//  Created by Aliya on 23.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    
    var makeView: MakeSushi? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        let scene = GameScene(size: view.frame.size)
        scene.makeSushi = makeView!
        let skView = SKView(frame: self.view.frame)
        view.addSubview(skView)
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill
        scene.size = self.view.frame.size
        skView.presentScene(scene)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
