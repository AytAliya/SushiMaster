//
//  MakeSushiViewController.swift
//  sushiApp
//
//  Created by Aliya on 22.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class MakeSushiViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView = UITableView()
    let screen = UIScreen.main.bounds
    var sushiArr: [() -> MakeSushi] = MakeSushi.fetchSushi()
    var items = ["Kappa Maki".localized(),"Salmon Takuan Maki".localized(),"Philadelphia".localized(),"Shrimp Nigiri".localized(),"Chuka Gunkan".localized()]
    
    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "sushiBg")
        backImage.clipsToBounds = true
        return backImage
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(backImage)
        tableSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = tableView.indexPathForSelectedRow
        {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableSettings(){
        let navBar = self.navigationController?.navigationBar.frame.size.height
        tableView = UITableView(frame: CGRect(x: 10, y: navBar!+8, width: screen.width-20, height: screen.height - 60), style: UITableViewStyle.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MakeSushiTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.separatorColor = UIColor.clear.withAlphaComponent(0)
        //tableView.allowsSelection = false
        self.view.addSubview(tableView)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MakeSushiTableViewCell
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = #colorLiteral(red: 0.4159462246, green: 0.4159462246, blue: 0.4159462246, alpha: 0.6034621147)
        cell.selectedBackgroundView = bgColorView
        
        cell.typeNameLabel.text = self.items[indexPath.row].localized()
        
        cell.typeNameLabel <- [
            CenterX().to(cell, .centerX),
            CenterY().to(cell, .centerY)
        ]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (screen.height - 60)/5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gameView = GameViewController()
        gameView.makeView = sushiArr[indexPath.row]()
        self.navigationController?.pushViewController(gameView, animated: true)
    }
    
}
