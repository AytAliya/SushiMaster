//
//  VideoTableViewCell.swift
//  sushiApp
//
//  Created by Aliya on 09.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class VideoTableViewCell: UITableViewCell {

    var sushiImg: UIImageView = {
        let img = UIImageView()
        img.contentMode =  UIViewContentMode.scaleAspectFill
        return img
    }()
    
    var typeNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.font = label.font.withSize(20)
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupViews(){
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        contentView.addSubviews(sushiImg, typeNameLabel)
        
        let screen = UIScreen.main.bounds
        
        typeNameLabel <- [
            CenterY(screen.height/1000),
            CenterX(screen.width/6)
        ]
    }

}
