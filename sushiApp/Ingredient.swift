//
//  Ingredient.swift
//  spriteTry
//
//  Created by Aliya on 21.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import Foundation
import SpriteKit

class Ingredient {
    
    var name: String
    var node: SKSpriteNode
    
    init(name: String, node: SKSpriteNode) {
        self.name = name
        self.node = node
    }
    
    static func fetchKappaMakiIngredients() -> [Ingredient]{
        return [
            Ingredient(name: "Nori", node: SKSpriteNode(imageNamed: "nori")),
            Ingredient(name: "Rice", node: SKSpriteNode(imageNamed: "rice")),
            Ingredient(name: "Cucumber", node: SKSpriteNode(imageNamed: "cucu")),
            Ingredient(name: "Roll up", node: SKSpriteNode())
        ]
    }
    
    static func fetchKappaMakiNodes() -> [SKSpriteNode]{
        return [
            SKSpriteNode(imageNamed: "nori"),
            SKSpriteNode(imageNamed: "rice"),
            SKSpriteNode(imageNamed: "cucu")
        ]
    }
    
    static func fetchPhiladelphiaIngredients() -> [Ingredient]{
        return [
            Ingredient(name: "Nori", node: SKSpriteNode(imageNamed: "nori")),
            Ingredient(name: "Rice", node: SKSpriteNode(imageNamed: "rice")),
            Ingredient(name: "Cream cheese Philadelphia", node: SKSpriteNode(imageNamed: "cream")),
            Ingredient(name: "Salmon", node: SKSpriteNode(imageNamed: "salmon")),
            Ingredient(name: "Roll up", node: SKSpriteNode())
        ]
    }
    
    static func fetchPhiladelphiaNodes() -> [SKSpriteNode]{
        return [
            SKSpriteNode(imageNamed: "nori"),
            SKSpriteNode(imageNamed: "rice"),
            SKSpriteNode(imageNamed: "cream"),
            SKSpriteNode(imageNamed: "salmon")
        ]
    }
    
    static func fetchSalmonIngredients() -> [Ingredient]{
        return [
            Ingredient(name: "Nori", node: SKSpriteNode(imageNamed: "nori")),
            Ingredient(name: "Rice", node: SKSpriteNode(imageNamed: "rice")),
            Ingredient(name: "Takuan", node: SKSpriteNode(imageNamed: "takuan")),
            Ingredient(name: "Salmon", node: SKSpriteNode(imageNamed: "salmon")),
            Ingredient(name: "Salad list", node: SKSpriteNode(imageNamed: "list")),
            Ingredient(name: "Roll up", node: SKSpriteNode())
        ]
    }
    
    static func fetchSalmonNodes() -> [SKSpriteNode]{
        return [
            SKSpriteNode(imageNamed: "nori"),
            SKSpriteNode(imageNamed: "rice"),
            SKSpriteNode(imageNamed: "takuan"),
            SKSpriteNode(imageNamed: "salmon"),
            SKSpriteNode(imageNamed: "list")
        ]
    }
    
    static func fetchNigiriIngredients() -> [Ingredient]{
        return [
            Ingredient(name: "Rice", node: SKSpriteNode(imageNamed: "rice")),
            Ingredient(name: "Shrimp", node: SKSpriteNode(imageNamed: "shrimp")),
            Ingredient(name: "Nori", node: SKSpriteNode(imageNamed: "nori"))
        ]
    }
    
    static func fetchNigiriNodes() -> [SKSpriteNode]{
        return [
            SKSpriteNode(imageNamed: "rice"),
            SKSpriteNode(imageNamed: "shrimp"),
            SKSpriteNode(imageNamed: "nori")
        ]
    }
    
    static func fetchGunkanIngredients() -> [Ingredient]{
        return [
            Ingredient(name: "Rice", node: SKSpriteNode(imageNamed: "rice")),
            Ingredient(name: "Nori", node: SKSpriteNode(imageNamed: "nori")),
            Ingredient(name: "Chuka", node: SKSpriteNode(imageNamed: "chuka"))
            
        ]
    }
    
    static func fetchGunkanNodes() -> [SKSpriteNode]{
        return [
            SKSpriteNode(imageNamed: "rice"),
            SKSpriteNode(imageNamed: "nori"),
            SKSpriteNode(imageNamed: "chuka")
        ]
    }
    
}
