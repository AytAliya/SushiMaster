//
//  SushiVideoTutorialsViewController.swift
//  sushiApp
//
//  Created by Aliya on 04.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import FirebaseStorage
import Firebase
import VGPlayer
import EasyPeasy

class SushiVideoTutorialsViewController: UIViewController{
    
    var sushiVideo:Video? = nil
    
    var player : VGPlayer = {
        let playerView = VGPlayerView()
        let playe = VGPlayer(playerView: playerView)
        return playe
    }()
    
    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "sushiBg")
        backImage.clipsToBounds = true
        return backImage
    }()
    
    var infoBackgroundLabel: UILabel = {
        let label = RoundedLabel()
        label.isHidden = false
        label.alpha = 1.0
        return label
    }()
    
    var about: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "about")
        return img
    }()
 
    var ingredients: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "ingredients")
        return img
    }()
    
    var additional: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "additional")
        return img
    }()
    
    var aboutInfo = InfoTextLabel()
    var inThisInfo = InfoTextLabel()
    var ingredientsInfo = InfoTextLabel()
    var additionalInfo = InfoTextLabel()
    
    let screen = UIScreen.main.bounds
    var storage = Storage.storage()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubviews(backImage, infoBackgroundLabel, about, ingredients, additional, aboutInfo, inThisInfo, ingredientsInfo, additionalInfo)
        setupTexts()
        setupConstraints()
        setupVideo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        (UIApplication.shared.delegate as? AppDelegate)?.blockRotation = true
    }

    // MARK: - Setting up
    func setupVideo(){
        let videoName = sushiVideo!.video
        (UIApplication.shared.delegate as? AppDelegate)?.blockRotation = false
        view.addSubview(self.player.displayView)
        
        self.player.backgroundMode = .suspend
        self.player.delegate = self
        self.player.displayView.delegate = self
        self.player.displayView.snp.makeConstraints { [weak self] (make) in
            guard let strongSelf = self else { return }
            make.top.equalTo(strongSelf.view.snp.top)
            make.left.equalTo(strongSelf.view.snp.left)
            make.right.equalTo(strongSelf.view.snp.right)
            make.height.equalTo(strongSelf.view.snp.width).multipliedBy(9.0/16.0)
        }
        self.player.displayView.closeButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.player.displayView.titleLabel.text = sushiVideo!.typeName.localized()
        
        
        let storageRef = storage.reference(forURL: "gs://sushiapp-889e8.appspot.com/\(videoName).mp4")
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let localURL = documentsURL.appendingPathComponent("\(videoName).mp4")
        if FileManager.default.fileExists(atPath: localURL.path) {
            self.play(url: localURL)
        } else {
            let downloadTask = storageRef.write(toFile: localURL) { url, error in
                if let error = error {
                    print("ERROR: \(error.localizedDescription)")
                } else {
                    self.play(url: url)
                }
            }
        }
    }
    
    func setupTexts(){
        aboutInfo.text = sushiVideo!.info.localized()
        inThisInfo.text = sushiVideo!.thisType.localized()
        ingredientsInfo.text = sushiVideo!.ingredients.localized()
        additionalInfo.text = sushiVideo!.additional.localized()
        if(screen.width<350){
            [aboutInfo, inThisInfo, ingredientsInfo, additionalInfo].forEach { lbl in
                lbl.font = UIFont(name: "Times New Roman", size: 14)
            }
        } else if(screen.width>400){
            [aboutInfo, inThisInfo, ingredientsInfo, additionalInfo].forEach { lbl in
                lbl.font = UIFont(name: "Times New Roman", size: 19)
            }
        }
        
        if(screen.height<481){
            [aboutInfo, inThisInfo, ingredientsInfo, additionalInfo].forEach { lbl in
                lbl.font = UIFont(name: "Times New Roman", size: 13)
            }
        }
    }
    
    func setupConstraints(){
        infoBackgroundLabel <- [
            Height(500),
            Width(screen.width-30),
            Top(self.view.bounds.width*(9.0/16.0) + 20),
            Left(15).to(view, .left)
        ]
        
        about <- [
            Width(40),
            Height(40),
            Left(10).to(infoBackgroundLabel, .left),
            Top(10).to(infoBackgroundLabel, .top)
        ]
        if(screen.width<350){
            about <- [
                Width(30),
                Height(30),
            ]
        }
        
        aboutInfo <- [
            Left(10).to(about, .right),
            Right(10).to(infoBackgroundLabel, .right),
            Top(10).to(infoBackgroundLabel, .top)
        ]
        
        inThisInfo <- [
            Left(10).to(about, .right),
            Right(10).to(infoBackgroundLabel, .right),
            Top(10).to(aboutInfo, .bottom)
        ]
        
        ingredients <- [
            Width(40),
            Height(40),
            Left(10).to(infoBackgroundLabel, .left),
            Top(10).to(inThisInfo, .bottom)
        ]
        if(screen.width<350){
            ingredients <- [
                Width(30),
                Height(30),
            ]
        }
        
        ingredientsInfo <- [
            Left(10).to(ingredients, .right),
            Right(10).to(infoBackgroundLabel, .right),
            Top(10).to(inThisInfo, .bottom)
        ]
        
        additional <- [
            Width(40),
            Height(40),
            Left(10).to(infoBackgroundLabel, .left),
            Top(10).to(ingredientsInfo, .bottom)
        ]
        if(screen.width<350){
            additional <- [
                Width(30),
                Height(30),
            ]
        }
        
        additionalInfo <- [
            Left(10).to(additional, .right),
            Right(10).to(infoBackgroundLabel, .right),
            Top(10).to(ingredientsInfo, .bottom)
        ]
    }
    
    // MARK: - Actions
    func back(){
        dismiss(animated: true, completion: nil)
    }
    
    func play(url: URL?) {
        self.player.replaceVideo(url!)
        self.player.play()
    }
    
}


extension SushiVideoTutorialsViewController: VGPlayerDelegate {
    func vgPlayer(_ player: VGPlayer, playerFailed error: VGPlayerError) {
        print(error)
    }
    func vgPlayer(_ player: VGPlayer, stateDidChange state: VGPlayerState) {
        print("player State ",state)
    }
    func vgPlayer(_ player: VGPlayer, bufferStateDidChange state: VGPlayerBufferstate) {
        print("buffer State", state)
    }
    
}

extension SushiVideoTutorialsViewController: VGPlayerViewDelegate {
    
    
    func vgPlayerView(_ playerView: VGPlayerView, willFullscreen fullscreen: Bool) {
        
    }
    func vgPlayerView(didTappedClose playerView: VGPlayerView) {
        if playerView.isFullScreen {
            playerView.exitFullscreen()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    func vgPlayerView(didDisplayControl playerView: VGPlayerView) {
    }
}

