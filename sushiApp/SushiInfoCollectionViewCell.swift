//
//  SushiInfoCollectionViewCell.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class SushiInfoCollectionViewCell: UICollectionViewCell {
    var sushiNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        label.font = label.font.withSize(20)
        return label
    }()
    
    var infoLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    var ingredients: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "ingredients")
        return img
    }()
    
    var about: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "about")
        return img
    }()
    
    var fact: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "fact")
        return img
    }()
    
    var additional: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "additional")
        return img
    }()
    
    var videoBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "videoIcon"), for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    var info1 = InfoTextLabel()
    var info2 = InfoTextLabel()
    var info3 = InfoTextLabel()
    var info4 = InfoTextLabel()
    var info5 = InfoTextLabel()
    
    var sushiNum: UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9611159581, green: 0.9611159581, blue: 0.9611159581, alpha: 0.696489726)
        return lbl
    }()
    
    var infoBackgroundLabel: UILabel = {
        let label = RoundedLabel()
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        contentView.addSubviews(sushiNameLabel, infoBackgroundLabel, info1, info2, info3, info4, ingredients, fact, about, additional, sushiNum, info5, videoBtn)
        
        sushiNum.font = UIFont(name: "American Typewriter", size: 25)
        sushiNameLabel.font = UIFont(name: "American Typewriter", size: 30)    }
}

class RoundedLabel: UILabel {
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        isHidden = true
        alpha = 0
        layer.masksToBounds = true
        layer.cornerRadius = 10.0
        backgroundColor = #colorLiteral(red: 0.9058823529, green: 0.9294117647, blue: 0.8784313725, alpha: 0.8525203339)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class InfoTextLabel: UILabel {
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        numberOfLines = 0
        textColor = .black
        textAlignment = .left
        font = UIFont(name: "Times New Roman", size: 18)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
