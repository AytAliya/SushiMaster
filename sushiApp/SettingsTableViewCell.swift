//
//  SettingsTableViewCell.swift
//  sushiApp
//
//  Created by Aliya on 03.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class SettingsTableViewCell: UITableViewCell {
    
    var btn = UIButton(type: .system)
    var line = UIView()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews(){
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        btn.setTitleColor(.white, for: .normal)
        line.backgroundColor = #colorLiteral(red: 0.4507766904, green: 0.4552398259, blue: 0.4552398259, alpha: 1)

        contentView.addSubviews(btn, line)
        
        btn <- Edges()
        
        line <- [
            Height(1),
            Bottom(),
            Left(),
            Right()
        ]
    }
}
