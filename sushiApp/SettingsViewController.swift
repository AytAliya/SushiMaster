//
//  SettingsViewController.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import Tactile
import EasyPeasy
import MessageUI
import Social
import MGInstagram
import Localize_Swift

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    
    var tableView: UITableView = UITableView()
    var image = UIImage(named: "shareImg")
    var instagram = MGInstagram()
    
    var ruBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "oval"), for: .normal)
        btn.addTarget(self, action: #selector(ruBtnAction), for: .touchUpInside)
        return btn
    }()
    
    var enBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "enOval"), for: .normal)
        btn.addTarget(self, action: #selector(enBtnAction), for: .touchUpInside)
        return btn
    }()
    
    var kzBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "kzOval"), for: .normal)
        btn.addTarget(self, action: #selector(kzBtnAction), for: .touchUpInside)
        return btn
    }()
    
    var items: [String] = ["Rate app".localized(), "Leave feedback".localized(), "Share in Instagram".localized(), "Share in Facebook".localized()]
    
    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "settingBack")
        backImage.clipsToBounds = true
        return backImage
    }()
    
    var thanks: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont(name: "Savoye LET", size: 30)
        label.text = "Special thanks to Ginza Express for providing sushi".localized()
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubviews(backImage, ruBtn, enBtn, kzBtn, thanks)
        setupConstraints()
        tableSettings()
              
        setText()
    }

    func setText() {
        items = ["Rate app".localized(), "Leave feedback".localized(), "Share in Instagram".localized(), "Share in Facebook".localized()]
        tableView.reloadData()
        thanks.text = "Special thanks to Ginza Express stuff for helping me in producing videos".localized()
    }

    func setupConstraints(){
        ruBtn <- [
            CenterX().to(view, .centerX),
            CenterY().to(view, .centerY),
            Height(82),
            Width(88)
        ]
        
        enBtn <- [
            CenterX(-65).to(ruBtn, .left),
            CenterY().to(view, .centerY),
            Height(82),
            Width(88)
        ]
        
        kzBtn <- [
            CenterX(65).to(ruBtn, .right),
            CenterY().to(view, .centerY),
            Height(82),
            Width(88)
        ]
        if(view.bounds.width<350){
            [ruBtn, enBtn, kzBtn].forEach { btn in
                btn <- [
                    Height(62),
                    Width(68)
                ]
            }
        }
        
        thanks <- [
            Bottom(10).to(view, .bottom),
            Left(15),
            Right(15)
        ]
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func tableSettings(){
        tableView = UITableView(frame: CGRect(x: 0, y: 40, width: view.bounds.width, height: view.bounds.height/2-40), style: UITableViewStyle.plain)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.separatorColor = UIColor.clear.withAlphaComponent(0)
        self.view.addSubview(tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingsTableViewCell
        
        cell.btn.setTitle(self.items[indexPath.row], for: .normal)
        if(view.bounds.width > 400){
            cell.btn.titleLabel?.font = UIFont(name: "Helvetica", size: 19)
        } else{
            cell.btn.titleLabel?.font = UIFont(name: "Helvetica", size: 17)
        }
        
        if(indexPath.row == 3) {
            cell.line.isHidden = true
        }
        
        cell.btn.on(.touchUpInside) { _ in
            if(indexPath.row == 0){
                self.rateApp()
            } else if(indexPath.row == 1){
                self.sendMail()
            } else if(indexPath.row == 2){
                self.insta()
            } else if(indexPath.row == 3){
                let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                fbShare.title="share"
                fbShare.add(UIImage(named: "shareImg"))
                self.present(fbShare, animated: true, completion: nil)
            }
        }
        return cell
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    func sendMail(){
        let composer = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail() {
            composer.mailComposeDelegate = self
            composer.setToRecipients(["ayt.aliya@gmail.com"])
            composer.setSubject("Sushi Master")
            composer.setMessageBody("", isHTML: false)
            present(composer, animated: true, completion: nil)
        }
    }
    
    func rateApp(){
        let appID = "1272743455"
        let urlStr = "itms-apps://itunes.apple.com/app/viewContentsUserReviews?id=\(appID)"
        UIApplication.shared.openURL(NSURL(string: urlStr)! as URL)
        }
    
    func insta(){
        if MGInstagram.isAppInstalled() && MGInstagram.isImageCorrectSize(image){
            self.instagram.post(image, in: self.view)
        }
        else {
            NSLog("Error Instagram is either not installed or image is incorrect size");
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (tableView.frame.height)/6
    }
    
    func ruBtnAction(){
        Localize.setCurrentLanguage("ru")
        setText()
    }
    
    func enBtnAction(){
        Localize.resetCurrentLanguageToDefault()
        setText()
    }
    
    func kzBtnAction(){
        Localize.setCurrentLanguage("kk")
        setText()
    }

}
