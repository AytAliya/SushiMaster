//
//  File.swift
//  spriteTry
//
//  Created by Aliya on 21.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class Panel: UIView {
    
    var max = 0
    var count = 0 {
        willSet{
            counter.text = "\(newValue)/\(max)"
            steps[count].isHidden = false
            checkMark[count].isHidden = false
            if(count+1==max){
                wellDone.isHidden = false
            }
        }
    }
    var ingredients: [Ingredient] = []
    var counter: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "Avenir", size: 23)
        return lbl
    }()
    
    var nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont(name: "Athelas", size: 30)
        lbl.textColor = #colorLiteral(red: 0.7607843137, green: 0.2784313725, blue: 0.2784313725, alpha: 1)
        return lbl
    }()
    
    var wellDone: UILabel = {
        let lbl = UILabel()
        lbl.text = "Well Done!!!".localized()
        lbl.font = UIFont(name: "Athelas", size: 30)
        lbl.textColor = #colorLiteral(red: 0.7607843137, green: 0.2784313725, blue: 0.2784313725, alpha: 1)
        lbl.isHidden = true
        return lbl
    }()
    
    
    lazy var steps: [UILabel] = {
        var labels:[UILabel] = []
        self.ingredients.forEach{ ing in
            let lbl = UILabel()
            lbl.text = ing.name.localized()
            lbl.isHidden = true
            lbl.font = UIFont(name: "Avenir", size: 17)
            labels.append(lbl)
        }
        return labels
    }()
    
    lazy var checkMark: [UIImageView] = {
        var imgArr: [UIImageView] = []
        self.ingredients.forEach{ ing in
            let img = UIImageView()
            img.image = UIImage(named: "check")
            img.isHidden = true
            imgArr.append(img)
        }
        return imgArr
    }()
    
    init(max: Int, ingr: [Ingredient], name: String){
        super.init(frame: UIScreen.main.bounds)
        self.max = max
        self.ingredients = ingr
        self.nameLabel.text = name.localized()
        setupViews()
        setupConstraints()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
        
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews(){
        addSubview(nameLabel)
        addSubview(counter)
        steps.forEach {
            addSubview($0)
        }
        checkMark.forEach{
            addSubview($0)
        }
        addSubview(wellDone)
    }
    
    func setupConstraints(){
        nameLabel <- [
            Top(3).to(self, .top),
            CenterX()
        ]
        
        counter <- [
            Top(1).to(nameLabel),
            CenterX()
        ]
        
        if let check = checkMark.first {
            check <- [
                Top(1).to(counter),
                Left(15),
                Size(20)
            ]
            
        }

        if(checkMark.count != 0){
            for i in 1 ..< checkMark.count {
                checkMark[i] <- [
                    Top(5).to(steps[i-1]),
                    Left(15),
                    Size(20)
                ]
            }
        }
        
        
        if let label = steps.first {
            label <- [
                Top(1).to(counter),
                Left(10).to(checkMark.first!),
                Right(10).to(self)
            ]
            
        }
        
        if(steps.count != 0){
            for i in 1 ..< steps.count {
                steps[i] <- [
                    Top(5).to(steps[i-1]),
                    Left(10).to(checkMark[i]),
                    Right(10).to(self)
                ]
            }
        }
        
        wellDone <- [
            Bottom(3).to(self),
            CenterX()
        ]
    }
    
    
}
