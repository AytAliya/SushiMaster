//
//  SushiInfoViewController.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import EasyPeasy

class SushiInfoViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var collectionView: UICollectionView!
    var sushi:Sushi? = nil
    
    var sushiImage: UIImageView = {
        let sushiImage = MyImageView()
        return sushiImage
    }()
    
    var arrowImage: UIImageView = {
        let arrowImage = MyImageView()
        arrowImage.image = UIImage(named: "arrow")
        return arrowImage
    }()

    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "sushiBg")
        backImage.clipsToBounds = true
        return backImage
    }()
    
    let layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return layout
    }()

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        collectionViewSettings()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView?.scrollToItem(at: IndexPath(row: 0, section: 0),
                                          at: .top,
                                          animated: true)
    }
    
    // MARK: - Setting up
    func setupViews(){
        sushiImage.image = UIImage(named: (sushi?.mainImgName)!)
        self.view.addSubviews(backImage, sushiImage, arrowImage)
    }
    
    func collectionViewSettings(){
        layout.itemSize = CGSize(width: view.frame.width, height: view.frame.height)
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.register(SushiInfoCollectionViewCell.self, forCellWithReuseIdentifier: "SushiCell")
        collectionView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        self.view.addSubview(collectionView)
    }

    func setupConstraints(){
        
        sushiImage <- [
            Height(50),
            Width(150),
            CenterX().to(view, .centerX),
            CenterY().to(view, .centerY)
        ]
        arrowImage <- [
            Height(10),
            Width(70),
            CenterX().to(view, .centerX),
            Bottom(30).to(view)
        ]
    }
    
    //MARK: - Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SushiCell", for: indexPath) as! SushiInfoCollectionViewCell
        if(indexPath.row == 0){
            cell.infoLabel.isHidden = true
            cell.sushiNameLabel.isHidden = true
            cell.about.isHidden = true
            cell.additional.isHidden = true
            cell.fact.isHidden = true
            cell.ingredients.isHidden = true
            //cell.backgroundColor = .red
            cell.sushiNum.text = "\(sushi!.order)/12"
            cell.sushiNum <- [
                Top(15),
                CenterX()
            ]
        } else if(indexPath.row == 1){
            cell.sushiNameLabel.text = sushi?.sushiName.localized()
            cell.sushiNameLabel.isHidden = false
            cell.infoBackgroundLabel.isHidden = false
            cell.sushiNum.isHidden = true
            cell.infoBackgroundLabel.alpha = 1
            cell.info1.text = sushi?.ingredients.localized()
            cell.info2.text = sushi?.info.localized()
            cell.info3.text = sushi?.fact.localized()
            cell.info4.text = sushi?.additional.localized()
            cell.info5.text = sushi?.videoType.localized()
            cell.videoBtn.isHidden = false
            cell.videoBtn.addTarget(self, action: #selector(showVideo), for: .touchUpInside)
            if(sushi?.additional == ""){
                cell.additional.isHidden = true
            }
            
            cell.infoBackgroundLabel <- [
                Height(view.bounds.height/1.2),
                Top(80),
                Left(view.bounds.width*30/414),
                Right(view.bounds.width*30/414),
            ]
            
            cell.ingredients <- [
                Width(view.bounds.width*40/414),
                Height(view.bounds.width*40/414),
                Left(10).to(cell.infoBackgroundLabel, .left),
                Top(10).to(cell.infoBackgroundLabel, .top)
            ]
            
            cell.info1 <- [
                Left(10).to(cell.ingredients, .right),
                Right(10).to(cell.infoBackgroundLabel, .right),
                Top(10).to(cell.infoBackgroundLabel, .top)
            ]
            
            cell.about <- [
                Width(view.bounds.width*40/414),
                Height(view.bounds.width*40/414),
                Left(10).to(cell.infoBackgroundLabel, .left),
                Top(13).to(cell.info1, .bottom)
            ]
            
            cell.info2 <- [
                Left(10).to(cell.about, .right),
                Right(10).to(cell.infoBackgroundLabel, .right),
                Top(13).to(cell.info1, .bottom)
            ]
            
            cell.fact <- [
                Width(view.bounds.width*40/414),
                Height(view.bounds.width*40/414),
                Left(10).to(cell.infoBackgroundLabel, .left),
                Top(13).to(cell.info2, .bottom)
            ]
            
            cell.info3 <- [
                Left(10).to(cell.fact, .right),
                Right(10).to(cell.infoBackgroundLabel, .right),
                Top(13).to(cell.info2, .bottom)
            ]
            
            cell.additional <- [
                Width(view.bounds.width*40/414),
                Height(view.bounds.width*40/414),
                Left(10).to(cell.infoBackgroundLabel, .left),
                Top(13).to(cell.info3, .bottom)
            ]
            
            cell.info4 <- [
                Left(10).to(cell.additional, .right),
                Right(10).to(cell.infoBackgroundLabel, .right),
                Top(13).to(cell.info3, .bottom)
            ]
            
            cell.info5 <- [
                Top(14).to(cell.info4, .bottom),
                Left(10).to(cell.infoBackgroundLabel, .left),
                //Right(10).to(cell.infoBackgroundLabel, .right)
            ]
            
            cell.videoBtn <- [
                Top(10).to(cell.info4, .bottom),
                Left(15).to(cell.info5, .right),
                Width(view.bounds.height*40/736),
                Height(view.bounds.height*40/736)
            ]
            
            cell.sushiNameLabel <- [
                Height(30),
                Top(20),
                CenterX()
            ]
            
            if(view.bounds.width<350){
                [cell.info1, cell.info2, cell.info3, cell.info4, cell.info5].forEach{info in
                    info.font = UIFont(name: "Times New Roman", size: 14)
                }
            }
            
            if(view.bounds.height<481){
                [cell.info1, cell.info2, cell.info3, cell.info4, cell.info5].forEach{info in
                    info.font = UIFont(name: "Times New Roman", size: 12)
                }
            }
            //cell.backgroundColor = .green
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: indexPath.row == 0 ? view.bounds.height - 100 : view.bounds.height - 150)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let diff = view.center.y - (view.bounds.height - (view.bounds.height - 200)) / 2
        let offset = scrollView.contentOffset.y
        let scrollRange = view.bounds.height - 250
        let constraintOffset = (offset * diff) / scrollRange

        arrowImage.isHidden = offset > 0
        sushiImage <- CenterY(-constraintOffset).to(view, .centerY)
    }
    
    func showVideo(){
        let videoView = SushiVideoTutorialsViewController()
        videoView.sushiVideo = (sushi?.video)!
        self.navigationController?.pushViewController(videoView, animated: true)
    }
    
}

class MyImageView: UIImageView {
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        contentMode =  UIViewContentMode.scaleAspectFill
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
