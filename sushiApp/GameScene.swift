//
//  GameScene.swift
//  sushiApp
//
//  Created by Aliya on 23.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit
import EasyPeasy

class GameScene: SKScene{
    
    var makeSushi: MakeSushi? = nil
    var sushiCopy: MakeSushi? = nil
    
    let background = SKSpriteNode(imageNamed: "backgr")
    
    var rollBtn: UIButton = {
        let btn = UIButton()
        btn.isHidden = true
        btn.backgroundColor = #colorLiteral(red: 0.8784313725, green: 0.6509803922, blue: 0.4156862745, alpha: 0.7818118579)
        btn.setTitle("Roll up".localized(), for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btn.layer.cornerRadius = 20.0
        return btn
    }()
    
    var panel: Panel = {
        let panel = Panel()
        panel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7544413527)
        return panel
    }()
    
    let board: SKSpriteNode = {
        let brd = SKSpriteNode(imageNamed: "wBoard")
        brd.zPosition = 2
        brd.anchorPoint = CGPoint(x: 0, y: 1)
        return brd
    }()
    
    var finalImg = UIImageView()
    var selectedNode: SKSpriteNode? = nil
    var maxPosition = 4
    var selectedPosition: CGPoint? = nil
    var nigiriGunkan = false
    
    override func didMove(to view: SKView) {
        sushiCopy = makeSushi
        nigiriGunkan = (sushiCopy!.name == "Nigiri" || sushiCopy!.name == "Gunkan") ? true : false
        panel = Panel(max: (sushiCopy!.max), ingr: sushiCopy!.ingredientsArr, name: sushiCopy!.name)
        panel.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7544413527)
        setupViews()
        setBackground()
        setConstraints()
    }
    
    func setupViews(){
        let sWidth = UIScreen.main.bounds.size.width
        let sHeight = UIScreen.main.bounds.size.height
        self.addChild(board)
        self.view?.addSubviews(panel, rollBtn, finalImg)
        var index = 0
        sushiCopy?.ingredients.forEach{ing in
            ing.zPosition = 3
            ing.anchorPoint = CGPoint(x: 1, y: 1)
            ing.position = CGPoint(x: sWidth-((sHeight*20)/667), y: sHeight-(sHeight * makeSushi!.distances[index]/667))
            ing.size = CGSize(width: (sHeight * makeSushi!.widths[index])/667, height: (sHeight * makeSushi!.heights[index])/667)
            self.addChild(ing)
            index += 1
        }
        self.view?.addSubview(rollBtn)
        rollBtn.addTarget(self, action: #selector(rollUp), for: .touchUpInside)
        finalImg.isHidden = true
    }
    
    func setConstraints(){
        let sHeight = UIScreen.main.bounds.size.height
        
        board.position = CGPoint(x: sHeight * 40/667, y: sHeight-(80*sHeight/667))
        board.size = CGSize(width: (sHeight * 200)/667, height: (sHeight * 200)/667)

        
        panel <- [
            Left(0),
            Right(0),
            Bottom(0),
            Height(sHeight*270/667)
        ]
        
        rollBtn <- [
            Left(sHeight*40/667),
            Width(sHeight*200/667),
            Height(sHeight*40/667),
            Bottom(sHeight*30/667).to(panel, .top)
        ]
        
        finalImg <- [
            CenterX().to(rollBtn),
            Width(sHeight*196/667),
            Height(sHeight*50/667),
            Top(sHeight*150/667)
        ]
        
        panel.nameLabel.font = UIFont(name: "Athelas", size: sHeight*30/667)
        panel.wellDone.font = UIFont(name: "Athelas", size: sHeight*29/667)
        panel.counter.font = UIFont(name: "Avenir", size: sHeight*23/667)
        panel.steps.forEach{step in
            step.font = UIFont(name: "Avenir", size: sHeight*17/667)
        }
        
        if(sHeight == 480){
            panel.nameLabel.font = UIFont(name: "Athelas", size: 19)
            panel.wellDone.font = UIFont(name: "Athelas", size: 19)
            panel.counter.font = UIFont(name: "Avenir", size: 15)
        }
    }

    func setBackground(){
        background.zPosition = 1
        background.position = CGPoint(x: 0, y: 0)
        background.size.width = self.size.width
        background.size.height = self.size.height
        background.anchorPoint = CGPoint(x: 0,y: 0)
        self.addChild(background)
    }
    
    func rollUp(){
        rollBtn.isEnabled = false
        board.zPosition = CGFloat(maxPosition+1)
        finalImg.isHidden = false
        finalImg.image = UIImage(named: makeSushi!.finalImg)
        panel.count += 1
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            sushiCopy?.ingredients.forEach{ing in
                if (ing.contains(location)){
                    selectedPosition = ing.position
                    selectedNode = ing
                    selectedNode?.zPosition = CGFloat(maxPosition)
                    maxPosition += 1
                }
            }
        }
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let sHeight = UIScreen.main.bounds.size.height
        for touch in touches {
            let location = touch.location(in: self)
            
            if selectedNode != nil {
                selectedNode?.position.y = max(location.y, panel.frame.size.height + (sHeight*70/667))
                selectedNode?.position.x = location.x
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let sHeight = UIScreen.main.bounds.size.height
        for touch in touches {
            let location = touch.location(in: self)
            if selectedNode != nil {
                if(board.contains(location) && selectedNode == sushiCopy?.ingredients.first){
                    let point = CGPoint(x: sHeight * 140/667, y: sHeight * 487/667)
                    selectedNode?.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                    selectedNode?.run(.move(to: point, duration: 0.1))
                    sushiCopy?.ingredients.remove(at: 0)
                    selectedNode?.texture = SKTexture(imageNamed: (sushiCopy?.imgEnd.first)!)
                    sushiCopy?.imgEnd.remove(at: 0)
                    selectedNode?.size = CGSize(width: sHeight * sushiCopy!.endWidth.first!/667, height: sHeight * sushiCopy!.endHeights.first!/667)
                    sushiCopy!.endWidth.remove(at: 0)
                    sushiCopy!.endHeights.remove(at: 0)
                    selectedNode = nil
                    panel.count += 1
                    if(sushiCopy?.ingredients.count == 0 && !nigiriGunkan){
                        rollBtn.isHidden = false
                    }
                } else {
                    selectedNode?.run(.move(to: selectedPosition!, duration: 0.3))
                }
            }
        }
    }
}
