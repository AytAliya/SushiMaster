//
//  MakeSushi.swift
//  
//
//  Created by Aliya on 21.08.17.
//
//

import Foundation
import SpriteKit

class MakeSushi{

    var name: String
    var ingredientsArr: [Ingredient]
    var ingredients: [SKSpriteNode]
    var imgEnd: [String]
    var max: Int
    var finalImg: String
    var heights: [CGFloat]
    var widths: [CGFloat]
    var distances: [CGFloat]
    var endHeights: [CGFloat]
    var endWidth: [CGFloat]
    
    init(name: String, ingredients: [SKSpriteNode], imgEnd: [String], max: Int, ingredientsArr: [Ingredient], finalImg: String, heights: [CGFloat], widths: [CGFloat], distances: [CGFloat], endHeights: [CGFloat], endWidth: [CGFloat]) {
        self.name = name
        self.ingredients = ingredients
        self.imgEnd = imgEnd
        self.max = max
        self.ingredientsArr = ingredientsArr
        self.finalImg = finalImg
        self.heights = heights
        self.widths = widths
        self.distances = distances
        self.endHeights = endHeights
        self.endWidth = endWidth
    }
    
    static var selectedIndex: Int? = nil
    
    
    static func fetchSushi() -> [() -> MakeSushi] {
        return [
            {() -> MakeSushi in MakeSushi(name: "Kappa Maki",
                      ingredients: Ingredient.fetchKappaMakiNodes(),
                      imgEnd: ["noriii", "riceNori", "cucuOn"],
                      max: 4,
                      ingredientsArr: Ingredient.fetchKappaMakiIngredients(),
                      finalImg: "kappaOn",
                      heights: [80, 70, 70],
                      widths: [60, 70, 50],
                      distances: [40, 162, 275],
                      endHeights: [154, 106, 25],
                      endWidth: [140, 138, 140])},
            {() -> MakeSushi in MakeSushi(name: "Salmon Takuan Maki",
                      ingredients: Ingredient.fetchSalmonNodes(),
                      imgEnd: ["noriii", "riceNori", "takuanOn", "salmonIn", "listOn"],
                      max: 6,
                      ingredientsArr: Ingredient.fetchSalmonIngredients(),
                      finalImg: "kappaOn",
                      heights: [80, 70, 55, 60, 64],
                      widths: [60, 70, 30, 70, 65],
                      distances: [20, 107, 184, 246, 313],
                      endHeights: [154, 106, 21, 21, 27],
                      endWidth: [140, 138, 140, 140, 140])},
            {() -> MakeSushi in MakeSushi(name: "Philadelphia",
                      ingredients: Ingredient.fetchPhiladelphiaNodes(),
                      imgEnd: ["noriii", "riceNori", "philaOn", "salmonIn"],
                      max: 5,
                      ingredientsArr: Ingredient.fetchPhiladelphiaIngredients(),
                      finalImg: "wSalmon",
                      heights: [80, 70, 75, 60],
                      widths: [60, 70, 76, 70],
                      distances: [20, 124, 218, 317],
                      endHeights: [154, 106, 25, 21],
                      endWidth: [140, 138, 140, 140])},
            {() -> MakeSushi in MakeSushi(name: "Nigiri",
                      ingredients: Ingredient.fetchNigiriNodes(),
                      imgEnd: ["niRice", "shrimp", "noriii"],
                      max: 3,
                      ingredientsArr: Ingredient.fetchNigiriIngredients(),
                      finalImg: "",
                      heights: [70, 70, 80],
                      widths: [70, 40, 60],
                      distances: [20, 152, 265],
                      endHeights: [70, 80, 12],
                      endWidth: [38, 47, 37])},
            {() -> MakeSushi in MakeSushi(name: "Gunkan",
                      ingredients: Ingredient.fetchGunkanNodes(),
                      imgEnd: ["niRice", "Oval", "chGunkan"],
                      max: 3,
                      ingredientsArr: Ingredient.fetchGunkanIngredients(),
                      finalImg: "",
                      heights: [70, 63, 80],
                      widths: [70, 64, 60],
                      distances: [40, 156, 265],
                      endHeights: [70, 70, 95],
                      endWidth: [38, 37, 55])}
        ]
    }
}
