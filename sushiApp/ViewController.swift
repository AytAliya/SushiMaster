//
//  ViewController.swift
//  sushiApp
//
//  Created by Aliya on 02.08.17.
//  Copyright © 2017 AytAliya. All rights reserved.
//

import UIKit
import AVFoundation
import EasyPeasy
import Floaty
import EasyTipView


class ViewController: UIViewController, FloatyDelegate {
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    var playerItem: AVPlayerItem!
    
    var backImage: UIImageView = {
        let backImage = MyImageView(frame: UIScreen.main.bounds)
        backImage.image = UIImage(named: "tutBg")
        backImage.clipsToBounds = true
        backImage.isHidden = true
        return backImage
    }()
    
    var names = ["Make", "Watch", "Learn"]
    
    lazy var floaty: Floaty = {
        let button = Floaty()
        button.buttonColor = #colorLiteral(red: 1, green: 0.6588235294, blue: 0.3254901961, alpha: 1)
        button.plusColor = .black
        button.overlayColor = UIColor.black.withAlphaComponent(0.9)
        button.addItem("Make", icon: UIImage(named: "allSushiIcon"), handler: { item in self.makeAction() })
        button.addItem("Watch", icon: UIImage(named: "videoIcon"), handler: { item in self.videoAction() })
        button.addItem("Learn", icon: UIImage(named: "infoIcon"), handler: { item in self.allSushiAction() })
        button.isHidden = true
        button.alpha = 0
        
        return button
    }()
    
    var settingsBtn: UIButton = {
        let btn = UIButton()
        btn.isHidden = true
        btn.alpha = 0
        btn.setImage(UIImage(named: "settingsIcon"), for: .normal)
        btn.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
        return btn
    }()
    
    var invBtn: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .gray
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 0
        return btn
    }()
    var sticks: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .blue
        btn.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI*0.06))
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 1
        return btn
    }()
    var vasabi: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .yellow
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 2
        return btn
    }()
    var imbir: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .white
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 3
        return btn
    }()
    var unagi: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .brown
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 4
        return btn
    }()
    var spicy: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .green
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 5
        return btn
    }()
    var soya: UIButton = {
        let btn = UIButton()
        //btn.backgroundColor = .red
        btn.addTarget(self, action: #selector(didTap(_:)), for: .touchUpInside)
        btn.tag = 6
        return btn
    }()
    
    var al:UIAlertController = {
        let alert = UIAlertController()
        alert.message = "Tap the elements on the screen to see the information"
        alert.view.tintColor = .red
        alert.view.frame.origin = CGPoint(x: 0, y: 0)
        return alert
    }()
    
    var infoBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "infoIcon"), for: .normal)
        btn.addTarget(self, action: #selector(showAlert), for: .touchUpInside)
        btn.isHidden = true
        btn.alpha = 0
        return btn
    }()
    
    var tipView: EasyTipView?
    var isIphone5 = UIScreen.main.bounds.width == 320
    var isIphonePlus = UIScreen.main.bounds.width == 414
    var isIphone6 = UIScreen.main.bounds.width == 375
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        invButtonSettings()
        floaty.fabDelegate = self
        //revealButtons()
    }
    
    
    
    func floatyOpened(_ floaty: Floaty) {
        tipView?.removeFromSuperview()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        var i=0;
        self.floaty.items.forEach{ item in
            item.title = names[i].localized()
            i+=1
        }
        
        tipView?.removeFromSuperview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    //MARK: - Setting up
    func setupViews(){
        view.addSubviews(backImage, floaty, settingsBtn, infoBtn)
        setupConstraints()
        let url = Bundle.main.url(forResource: "backgr1", withExtension: "mp4")
        playVideo(url: url!)
    }
    
    func setupConstraints(){
        settingsBtn <- [
            Left(10),
            Bottom(15),
            Size(30)
        ]
        
        infoBtn <- [
            Height(view.bounds.width*20/375),
            Width(view.bounds.width*20/375),
            Top(15),
            Left(10)
        ]
    }
    
    func invButtonSettings(){
        view.addSubviews(invBtn, soya, sticks, vasabi, imbir, unagi, spicy)
        invBtn <- [
            isIphone5 ? Height(70) : Height(90),
            isIphone5 ? Width(70) : (isIphone6 ? Width(80) : Width(90)),
            isIphone5 ? Top(145) : (isIphone6 ? Top(170) : Top(190)),
            isIphone5 ? Left(170) : (isIphone6 ? Left(200) : Left(220))
        ]
        soya <- [
            isIphone5 ? Height(80) : (isIphone6 ? Height(90) : Height(100)),
            isIphone5 ? Width(65) : Width(80),
            isIphone5 ? Top(340) : (isIphone6 ? Top(410) : Top(440)),
            Left(0)
        ]
        sticks <- [
            isIphonePlus ? Height(340) : Height(350),
            Width(30),
            isIphone5 ? Top(315) : (isIphone6 ? Top(372) : Top(410)),
            isIphone5 ? Left(50) : (isIphone6 ? Left(75) : Left(90))
        ]
        vasabi <- [
            Height(30),
            isIphone5 ? Width(70) : (isIphone6 ? Width(75) : Width(90)),
            isIphone5 ? Top(295) : (isIphone6 ? Top(350) : Top(385)),
            isIphone5 ? Left(160) : (isIphone6 ? Left(200) : Left(210))
        ]
        spicy <- [
            isIphone5 ? Height(70) : (isIphone6 ? Height(80) : Height(90)),
            isIphone5 ? Width(70) : (isIphone6 ? Width(80) : Width(90)),
            isIphone5 ? Top(245) : (isIphone6 ? Top(290) : Top(310)),
            isIphone5 ? Left(85) : (isIphone6 ? Left(100) : Left(110))
        ]
        imbir <- [
            isIphonePlus ? Height(55) : Height(50),
            isIphonePlus ? Width(55) : Width(50),
            isIphone5 ? Top(280) : (isIphone6 ? Top(330) : Top(370)),
            isIphone5 ? Left(235) : (isIphone6 ? Left(280) : Left(310))
        ]
        unagi <- [
            Height(100),
            Width(90),
            isIphone5 ? Top(25) : Top(30),
            isIphone5 ? Left(275) : (isIphone6 ? Left(315) : Left(340))
        ]
        
        if(UIScreen.main.bounds.height == 480) {
            invBtn <- [
                Height(65),
                Width(65),
                Top(120),
                Left(165)
            ]
            soya <- [
                Height(70),
                Width(65),
                Top(290),
                Left(15)
            ]
            sticks <- [
                Height(350),
                Width(30),
                Top(265),
                Left(60)
            ]
            vasabi <- [
                Height(25),
                Width(55),
                Top(250),
                Left(165)
            ]
            spicy <- [
                Height(60),
                Width(60),
                Top(205),
                Left(100)
            ]
            imbir <- [
                Height(40),
                Width(40),
                Top(235),
                Left(225)
            ]
            unagi <- [
                Height(80),
                Width(90),
                Top(15),
                Left(255)
            ]
        }
    }
    
    func didTap(_ sender: UIButton) {
        tipView?.removeFromSuperview()
        tipView = nil
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 15)!
        preferences.drawing.foregroundColor = UIColor.black
        preferences.drawing.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.8392156863, blue: 0.8196078431, alpha: 1)
        preferences.drawing.borderWidth = 5
        preferences.drawing.arrowHeight = 8
        preferences.drawing.borderColor = #colorLiteral(red: 0.4507766904, green: 0.4552398259, blue: 0.4552398259, alpha: 1)
        EasyTipView.globalPreferences = preferences
        sender.isSelected = !sender.isSelected
        
        tipView = EasyTipView(text: "Peanut sauce is been used in many japanese cuisine meals. The recipe of the sauce consists of peanut, cashew and etc!", preferences: preferences)
        
        if !sender.isSelected {
            tipView?.dismiss()
            return
        }
        switch sender.tag {
        case 0:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
            tipView = EasyTipView(text: "Peanut sauce is been used in many japanese cuisine meals. It is prepared from peanuts, cashews with the addition of other elements.".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: invBtn, withinSuperview: view)
        case 1:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.left
            tipView = EasyTipView(text: "Food chopsticks are traditional cutlery in East Asia. In Japan these chopsticks are called \"Hashi\".".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: sticks, withinSuperview: view)
        case 2:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.left
            tipView = EasyTipView(text: "Wasabi is an extremely spicy condiment of japanese origin. Is made from grinding the stem of the Japanese horseradish plant with a sharp specific smell.".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: vasabi, withinSuperview: view)
        case 3:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
            tipView = EasyTipView(text: "Ginger - Sweet, sliced young ginger, which was marinated with the addition of vinegar and sugar. It is used to smash the taste between different sushi.".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: imbir, withinSuperview: view)
        case 4:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
            tipView = EasyTipView(text: "Unagi sauce - a thick sauce with a pleasant aftertaste, is a classic Japanese dressing for the eel".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: unagi, withinSuperview: view)
        case 5:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
            tipView = EasyTipView(text: "Spicy sauce (kimchi) - Hot sauce which for preparation requires soy sauce, spicy Korean cabbage (kimchi), mayonnaise and chili pepper.".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: spicy, withinSuperview: view)
            
        case 6:
            preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.right
            tipView = EasyTipView(text: "Soy sauce is one of the main ingredients of Asian cuisine. A real soy sauce for rolls is cooked for several months on the basis of soybeans".localized(), preferences: preferences)
            tipView?.show(animated: true, forView: soya, withinSuperview: view)
        default:
            print("default")
        }
    }

    
    // MARK: - Actions
    func playVideo(url: URL) {
        playerItem = AVPlayerItem(asset: AVURLAsset(url: url))
        NotificationCenter.default.addObserver(self, selector: #selector(didEndPlayingVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.frame = view.layer.frame
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        view.layer.insertSublayer(playerLayer, at: 0)
        player.play()
        
    }
    
    func showAlert() {
        let messageFont = [NSFontAttributeName: UIFont(name: "AvenirNext-Bold", size: 17.0)!]
        let messageAttrString = NSMutableAttributedString(string: "Tap the elements on the screen to see the information".localized(), attributes: messageFont)
        al.setValue(messageAttrString, forKey: "attributedMessage")
        self.present(al, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { _ in self.al.dismiss(animated: true, completion: nil)} )
    }
    
    func didEndPlayingVideo() {
        backImage.isHidden = false
        revealButtons()
    }
    
    func revealButtons() {
        floaty.isHidden = false
        settingsBtn.isHidden = false
        infoBtn.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
                self.floaty.alpha = 1
                self.settingsBtn.alpha = 1
                self.infoBtn.alpha = 1
            })
    }

    func allSushiAction() {
        let allSushiView = AllSushiPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        self.navigationController?.pushViewController(allSushiView, animated: true)
    }
    func videoAction() {
        let tutorialView = SushiTypesViewController()
        self.navigationController?.pushViewController(tutorialView, animated: true)
    }
    func settingsAction() {
        let settingsView = SettingsViewController()
        self.navigationController?.pushViewController(settingsView, animated: true)
    }
    func makeAction() {
        let makeView = MakeSushiViewController()
        self.navigationController?.pushViewController(makeView, animated: true)
    }

}

extension UIView {
    func addSubviews(_ views: UIView...) {
        views.forEach { self.addSubview($0) }
    }
}

