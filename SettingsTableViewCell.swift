//
//  SettingsTableViewCell.swift
//  
//
//  Created by Aliya on 03.08.17.
//
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    var btn = UIButton()
    var line = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        myLabel.backgroundColor = UIColor.yellowColor()
        self.contentView.addSubview(myLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    

}
